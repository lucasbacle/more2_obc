/*
 * payload.h
 *
 *  Created on: 7 mai 2020
 *      Author: lucas bacle
 */

#ifndef INC_PAYLOAD_H_
#define INC_PAYLOAD_H_

#include "main.h"

uint8_t payload_init(void);
void payload_start(void);
uint16_t get_payload_data(void);
void payload_stop(void);

#endif /* INC_PAYLOAD_H_ */
