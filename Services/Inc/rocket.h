/*
 * rocket.h
 *
 *  Created on: 7 mai 2020
 *      Author: lucas bacle
 */

#ifndef INC_ROCKET_H_
#define INC_ROCKET_H_

#include "main.h"

uint8_t rocket_init(void);
uint8_t rocket_is_started(void);
uint8_t rocket_has_landed(void);
uint8_t rocket_receive(void);
void rocket_store(void);
void rocket_transmit(void);

#endif /* INC_ROCKET_H_ */
