/*
 * time.h
 *
 *  Created on: 7 mai 2020
 *      Author: lucas bacle
 */

#ifndef INC_TIME_H_
#define INC_TIME_H_

#include "main.h"

uint8_t time_init(void);
uint32_t time_elapsed(void);
void time_reset(void);

#endif /* INC_TIME_H_ */
