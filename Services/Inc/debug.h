/*
 * debug.h
 *
 *  Created on: 7 mai 2020
 *      Author: lucas bacle
 */

#ifndef INC_DEBUG_H_
#define INC_DEBUG_H_

#include "main.h"

int __io_putchar(int ch);

#endif /* INC_DEBUG_H_ */
