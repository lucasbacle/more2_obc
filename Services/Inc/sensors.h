#ifndef INC_SENSORS_H_
#define INC_SENSORS_H_

#include "main.h"

//DEFINES are here for example, replace with good values

#define Temperature_PIN_SS GPIO_PIN_0
#define Temperature_PORT_SS GPIOB

#define Pressure_PIN_SS GPIO_PIN_1
#define Pressure_PORT_SS GPIOB

#define Accel_PIN_SS GPIO_PIN_2
#define Accel_PORT_SS GPIOB

// Public :
uint8_t sensors_init(void);
int get_temperature(void);
int get_pressure(void);
void get_acceleration(int *pResult);

// Private :
void SPI_Init(SPI_TypeDef * spiHandle);
void TemperatureSensor_Init(void);
void PressureSensor_Init(void);
void AccelSensor_Init(void);

#endif /* INC_SENSORS_H_ */
