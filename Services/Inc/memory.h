/*
 * memory.h
 *
 *  Created on: 7 mai 2020
 *      Author: lucas bacle
 */

#ifndef INC_MEMORY_H_
#define INC_MEMORY_H_

#include "main.h"

uint8_t memory_init(void);
void memory_store(void);

#endif /* INC_MEMORY_H_ */
