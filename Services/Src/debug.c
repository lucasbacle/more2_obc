/*
 * debug.c
 *
 *  Created on: 7 mai 2020
 *      Author: lucas bacle
 */

#include "debug.h"
#include "stdio.h"

UART_HandleTypeDef UartHandle = { 0 };

int __io_putchar(int ch) {
	HAL_UART_Transmit(&UartHandle, (uint8_t*) &ch, 1, 0xFFFF);
	return ch;
}
