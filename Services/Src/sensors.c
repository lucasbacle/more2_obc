#include "sensors.h"
#include "spi.h"

SPI_HandleTypeDef hspi_masterReceive;

uint8_t sensors_init(void) {
	SPI_Init(SPI1);
	TemperatureSensor_Init();
	PressureSensor_Init();
	AccelSensor_Init();
	return 1;
}

//Init of TemperatureSensor GPIO (for slave select)
void TemperatureSensor_Init(void) {

	GPIO_InitTypeDef GPIO_InitStruct = { 0 };

	__HAL_RCC_GPIOB_CLK_ENABLE();

	HAL_GPIO_WritePin(Temperature_PORT_SS, Temperature_PIN_SS, GPIO_PIN_SET);

	GPIO_InitStruct.Pin = Temperature_PIN_SS;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

}

//Init of PressureSensor GPIO (for slave select)
void PressureSensor_Init(void) {

	GPIO_InitTypeDef GPIO_InitStruct = { 0 };

	__HAL_RCC_GPIOB_CLK_ENABLE();

	HAL_GPIO_WritePin(Pressure_PORT_SS, Pressure_PIN_SS, GPIO_PIN_SET);

	GPIO_InitStruct.Pin = Pressure_PIN_SS;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

}

//Init of AccelSensor GPIO (for slave select)
void AccelSensor_Init(void) {

	GPIO_InitTypeDef GPIO_InitStruct = { 0 };

	__HAL_RCC_GPIOB_CLK_ENABLE();

	HAL_GPIO_WritePin(Accel_PORT_SS, Accel_PIN_SS, GPIO_PIN_SET);

	GPIO_InitStruct.Pin = Accel_PIN_SS;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

}

//Init of SPI (MOSI,MISO,CLK)
void SPI_Init(SPI_TypeDef *SPI) {

	//Init of SPI structure
	hspi_masterReceive = *(SPI_HandleTypeDef*) SPI_Init_MasterFullDuplex();

	//Init of MOSI, MISO, CLK on GPIO
	HAL_SPI_MspInit(&hspi_masterReceive);

}

int get_temperature(void) {

	uint16_t size = 8; //Set real information size of temperature sensor

	uint8_t data[size];

	SPI_Receive(&hspi_masterReceive,
	Temperature_PORT_SS, Temperature_PIN_SS, data, size, 1000);

	//Pas sur, a tester
	int returnData = *data;

	return returnData;

}

int get_pressure(void) {

	uint16_t size = 1; //16 bits

	uint8_t data[size];

	SPI_Receive(&hspi_masterReceive,
	Pressure_PORT_SS, Pressure_PIN_SS, data, size, 1000);

	//Pas sur, a tester
	int returnData = *data;

	return returnData;

}

void get_acceleration(int *pResult) {

	//Ce que l'ont doit ecrire
	//bit 0 : write mode = 0
	//bit 1 : MS bit = 0 (not increment address, 1 if
	// increment address in multiple writing)
	//bit 2-7 : ad address = address field of the indexed register

	/*On doit lire OUT_X_L = 0x28
	 * OUT_X_H = 0x29
	 * OUT_Y_L = 0x2A
	 * OUT_Y_H = 0x2B
	 * OUT_Z_L = 0x2C
	 * OUT_Z_H = 0x2D
	 */

	int tabADadr[6] = { 0x28, 0x29, 0x2A, 0x2B, 0x2C, 0x2D };

	int i;

	for (i = 0; i < 6; i++) {
		tabADadr[i]++; //Mise a 1 du bit READ
	}

	for (i = 0; i < 6; i++) {

		int tabTemp[2];

		uint8_t wSize = 8;
		uint8_t rSize = 8;

		uint8_t wData = tabADadr[i];
		uint8_t rData[rSize];

		SPI_TransmitReceive(&hspi_masterReceive,
		Accel_PORT_SS, Accel_PIN_SS, &wData, rData, wSize, rSize, 1000);

		/* SPI_Transmit(&hspi_masterReceive,
		 Pressure_PORT_SS, Pressure_PIN_SS,
		 wData, wSize,1000);

		 SPI_Receive(&hspi_masterReceive,
		 Pressure_PORT_SS, Pressure_PIN_SS,
		 rData, rSize,1000); */

		tabTemp[i % 2] = *rData;
		if (i % 2 == 1) {
			pResult[i / 2] = (tabTemp[0] << rSize) + tabTemp[1];
		}
	}

}
