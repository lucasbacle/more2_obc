/*
 * payload.c
 *
 *  Created on: 7 mai 2020
 *      Author: lucas bacle
 */

#include "payload.h"

uint8_t payload_init(void) {
	return 0;
}

void payload_start(void) {

}

uint16_t get_payload_data(void) {
	return 0xFFFF;
}

void payload_stop(void) {

}
