/*
 * rocket.c
 *
 *  Created on: 7 mai 2020
 *      Author: lucas bacle
 */

#include "rocket.h"

uint8_t rocket_init(void) {
	return 0;
}

uint8_t rocket_is_started(void) {
	return 0;
}

uint8_t rocket_has_landed(void) {
	return 0;
}

void rocket_store(void) {

}

uint8_t rocket_receive(void) {
	return 0;
}

void rocket_transmit(void) {

}
