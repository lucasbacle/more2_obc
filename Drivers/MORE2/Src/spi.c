/**
 ******************************************************************************
 * File Name          : SPI.c
 * Description        : This file provides code for the configuration
 *                      of the SPI instances.
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
 * All rights reserved.</center></h2>
 *
 * This software component is licensed by ST under Ultimate Liberty license
 * SLA0044, the "License"; You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *                             www.st.com/SLA0044
 *
 ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/
#include "spi.h"

/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

SPI_HandleTypeDef hspi;

/* SPI1 init function */
SPI_HandleTypeDef* SPI_Init_MasterFullDuplex(void) {

	hspi.Instance = SPI1;
	hspi.Init.Mode = SPI_MODE_MASTER;
	hspi.Init.Direction = SPI_DIRECTION_2LINES;
	hspi.Init.DataSize = SPI_DATASIZE_8BIT;
	hspi.Init.CLKPolarity = SPI_POLARITY_LOW;
	hspi.Init.CLKPhase = SPI_PHASE_1EDGE;
	hspi.Init.NSS = SPI_NSS_SOFT;
	hspi.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_2;
	hspi.Init.FirstBit = SPI_FIRSTBIT_MSB;
	hspi.Init.TIMode = SPI_TIMODE_DISABLE;
	hspi.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
	hspi.Init.CRCPolynomial = 10;
	if (HAL_SPI_Init(&hspi) != HAL_OK) {
		Error_Handler();
	}

	return &hspi;

}

void HAL_SPI_MspInit(SPI_HandleTypeDef *spiHandle) {

	GPIO_InitTypeDef GPIO_InitStruct = { 0 };
	if (spiHandle->Instance == SPI1) {
		/* USER CODE BEGIN SPI1_MspInit 0 */

		/* USER CODE END SPI1_MspInit 0 */
		/* SPI1 clock enable */
		__HAL_RCC_SPI1_CLK_ENABLE();

		__HAL_RCC_GPIOA_CLK_ENABLE();
		__HAL_RCC_GPIOB_CLK_ENABLE();
		/**SPI1 GPIO Configuration
		 PA5     ------> SPI1_SCK
		 PA6     ------> SPI1_MISO
		 PB5     ------> SPI1_MOSI
		 */
		GPIO_InitStruct.Pin = GPIO_PIN_5 | GPIO_PIN_6;
		GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
		GPIO_InitStruct.Pull = GPIO_NOPULL;
		GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
		GPIO_InitStruct.Alternate = GPIO_AF5_SPI1;
		HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

		GPIO_InitStruct.Pin = GPIO_PIN_5;
		GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
		GPIO_InitStruct.Pull = GPIO_NOPULL;
		GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
		GPIO_InitStruct.Alternate = GPIO_AF5_SPI1;
		HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

		/* USER CODE BEGIN SPI1_MspInit 1 */

		/* USER CODE END SPI1_MspInit 1 */
	}
}

void HAL_SPI_MspDeInit(SPI_HandleTypeDef *spiHandle) {

	if (spiHandle->Instance == SPI1) {
		/* USER CODE BEGIN SPI1_MspDeInit 0 */

		/* USER CODE END SPI1_MspDeInit 0 */
		/* Peripheral clock disable */
		__HAL_RCC_SPI1_CLK_DISABLE();

		/**SPI1 GPIO Configuration
		 PA5     ------> SPI1_SCK
		 PA6     ------> SPI1_MISO
		 PB5     ------> SPI1_MOSI
		 */
		HAL_GPIO_DeInit(GPIOA, GPIO_PIN_5 | GPIO_PIN_6);

		HAL_GPIO_DeInit(GPIOB, GPIO_PIN_5);

		/* USER CODE BEGIN SPI1_MspDeInit 1 */

		/* USER CODE END SPI1_MspDeInit 1 */
	}
}

/* USER CODE BEGIN 1 */
void SPI_TransmitReceive(SPI_HandleTypeDef *SPI_Struct, GPIO_TypeDef *GPIO,
		uint16_t GPIO_PIN, uint8_t *pdataTx, uint8_t *pdataRx, uint16_t sizeTx,
		uint16_t sizeRx, uint32_t timeout) {

	HAL_GPIO_WritePin(GPIO, GPIO_PIN, GPIO_PIN_RESET);
	uint32_t sizeTotal = sizeTx + sizeRx;
	HAL_SPI_TransmitReceive(SPI_Struct, pdataTx, pdataRx, sizeTotal, timeout);
	HAL_GPIO_WritePin(GPIO, GPIO_PIN, GPIO_PIN_SET);

}

//GPIO and GPIO_PIN are the SlaveSelect GPIO information
void SPI_Receive(SPI_HandleTypeDef *SPI_Struct, GPIO_TypeDef *GPIO,
		uint16_t GPIO_PIN, uint8_t *pdata, uint16_t size, uint32_t timeout) {

	HAL_GPIO_WritePin(GPIO, GPIO_PIN, GPIO_PIN_RESET);
	HAL_SPI_Receive(SPI_Struct, pdata, size, timeout);
	HAL_GPIO_WritePin(GPIO, GPIO_PIN, GPIO_PIN_SET);

}

//GPIO and GPIO_PIN are the SlaveSelect GPIO information
void SPI_Write(SPI_HandleTypeDef *SPI_Struct, GPIO_TypeDef *GPIO,
		uint16_t GPIO_PIN, uint8_t *pdata, uint16_t size, uint32_t timeout) {

	HAL_GPIO_WritePin(GPIO, GPIO_PIN, GPIO_PIN_RESET);
	HAL_SPI_Transmit(SPI_Struct, pdata, size, timeout);
	HAL_GPIO_WritePin(GPIO, GPIO_PIN, GPIO_PIN_SET);

}

/* USER CODE END 1 */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
