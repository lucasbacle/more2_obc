/**
 ******************************************************************************
 * File Name          : SPI.h
 * Description        : This file provides code for the configuration
 *                      of the SPI instances.
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
 * All rights reserved.</center></h2>
 *
 * This software component is licensed by ST under Ultimate Liberty license
 * SLA0044, the "License"; You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *                             www.st.com/SLA0044
 *
 ******************************************************************************
 */
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __spi_H
#define __spi_H
#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

extern SPI_HandleTypeDef hspi;

/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

SPI_HandleTypeDef* SPI_Init_MasterFullDuplex(void);

/* USER CODE BEGIN Prototypes */
/**
 * @brief  Listen for incoming data and fill data array given as parameter.
 * @retval None
 */
void SPI_TransmitReceive(SPI_HandleTypeDef *SPI_Struct, GPIO_TypeDef *GPIO,
		uint16_t GPIO_PIN, uint8_t *pdataTx, uint8_t *pdataRx, uint16_t sizeTx,
		uint16_t sizeRx, uint32_t timeout);

void SPI_Receive(SPI_HandleTypeDef *SPI_Struct, GPIO_TypeDef *GPIO,
		uint16_t GPIO_PIN, uint8_t *pdata, uint16_t Size, uint32_t timeout);

void SPI_Write(SPI_HandleTypeDef *SPI_Struct, GPIO_TypeDef *GPIO,
		uint16_t GPIO_PIN, uint8_t *pdata, uint16_t size, uint32_t timeout);

/* USER CODE END Prototypes */

#ifdef __cplusplus
}
#endif
#endif /*__ spi_H */

/**
 * @}
 */

/**
 * @}
 */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
